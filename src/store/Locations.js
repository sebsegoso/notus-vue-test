export default {
  namespaced: true,
  state: {
    locationList: null,
  },
  mutations: {
    setLocations(state, newLocationList) {
      state.locationList = newLocationList;
    },
  },
  actions: {
    UPDATE_LOCATIONS({ commit }) {
      return new Promise((resolve) => {
        fetch("http://localhost:3004/locations")
          .then((response) => response.json())
          .then((data) => {
            commit("setLocations", data);
            resolve(data);
          })
          .catch((err) => {
            console.log(err);
          });
      });
    },
  },
};
