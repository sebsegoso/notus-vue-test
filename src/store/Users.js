import axios from "axios";

export default {
  namespaced: true,
  state: {
    userList: null,
  },
  mutations: {
    setUsers(state, newUserList) {
      state.userList = newUserList;
    },
  },
  actions: {
    GET_USERS({ commit }) {
      return new Promise((resolve) => {
        fetch("http://localhost:3004/users")
          .then((response) => response.json())
          .then((data) => {
            commit("setUsers", data);
            resolve(data);
          })
          .catch((err) => {
            console.log(err);
          });
      });
    },
    async CREATE_USER({ dispatch }, newUser) {
      try {
        await axios.post("http://localhost:3004/users", newUser).then(() => {
          dispatch("GET_USERS");
        });

        alert("Usuario creado");
      } catch (error) {
        alert(error.message);
      }
    },
    async EDIT_USER({ dispatch }, editedUser) {
      try {
        await axios
          .put(`http://localhost:3004/users/${editedUser.id}`, editedUser)
          .then(() => {
            dispatch("GET_USERS");
          });

        alert("Usuario editado");
      } catch (error) {
        alert(error.message);
      }
    },
    async DELETE_USER({ dispatch }, id) {
      try {
        await axios.delete(`http://localhost:3004/users/${id}`).then(() => {
          dispatch("GET_USERS");
        });
      } catch (error) {
        alert(error.message);
      }
    },
  },
  getters: {
    userDataById: (state) => (id) => {
      return state.userList.find((u) => u.id === id);
    },
  },
};
