export default {
  namespaced: true,
  state: {
    turnList: null,
  },
  mutations: {
    setTurns(state, newTurnList) {
      state.turnList = newTurnList;
    },
  },
  actions: {
    UPDATE_TURNS({ commit }) {
      return new Promise((resolve) => {
        fetch("http://localhost:3004/turns")
          .then((response) => response.json())
          .then((data) => {
            commit("setTurns", data);
            resolve(data);
          })
          .catch((err) => {
            console.log(err);
          });
      });
    },
  },
};
