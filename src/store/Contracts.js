export default {
  namespaced: true,
  state: {
    contractList: null,
  },
  mutations: {
    setContracts(state, newContractList) {
      state.contractList = newContractList;
    },
  },
  actions: {
    UPDATE_CONTRACTS({ commit }) {
      return new Promise((resolve) => {
        fetch("http://localhost:3004/contracts")
          .then((response) => response.json())
          .then((data) => {
            commit("setContracts", data);
            resolve(data);
          })
          .catch((err) => {
            console.log(err);
          });
      });
    },
  },
};
