export default {
  namespaced: true,
  state: {
    turnTemplatesList: null,
  },
  mutations: {
    setTurnTemplates(state, newTurnTemplatesList) {
      state.turnTemplatesList = newTurnTemplatesList;
    },
  },
  actions: {
    UPDATE_TURN_TEMPLATES({ commit }) {
      return new Promise((resolve) => {
        fetch("http://localhost:3004/turnTemplates")
          .then((response) => response.json())
          .then((data) => {
            commit("setTurnTemplates", data);
            resolve(data);
          })
          .catch((err) => {
            console.log(err);
          });
      });
    },
  },
  getters: {
    turnTemplateById: (state) => (id) => {
      const { turnTemplatesList } = state;
      const turn = turnTemplatesList.find((t) => t.id === id);
      return turn;
    },
  },
};
