import Vue from "vue";
import Vuex from "vuex";
import Users from "./Users";
import Locations from "./Locations";
import Positions from "./Positions";
import Contracts from "./Contracts";
import Turns from "./Turns";
import TurnTemplates from "./TurnTemplates";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { Users, Locations, Positions, Contracts, Turns, TurnTemplates },
});
