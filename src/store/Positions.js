export default {
  namespaced: true,
  state: {
    positionList: null,
  },
  mutations: {
    setPositions(state, newPositionList) {
      state.positionList = newPositionList;
    },
  },
  actions: {
    UPDATE_POSITIONS({ commit }) {
      return new Promise((resolve) => {
        fetch("http://localhost:3004/positions")
          .then((response) => response.json())
          .then((data) => {
            commit("setPositions", data);
            resolve(data);
          })
          .catch((err) => {
            console.log(err);
          });
      });
    },
  },
};
